package com.coastalpol.coastalpol;

import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AboutActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView logoTV;
    private Typeface circulat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        circulat = Typeface.createFromAsset(getAssets(), "fonts/circulat.ttf");
        logoTV = (TextView) findViewById(R.id.logo_tentang_tv);

        logoTV.setTypeface(circulat);
    }
}
