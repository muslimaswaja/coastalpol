package com.coastalpol.coastalpol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.coastalpol.coastalpol.Model.Heatmap;
import com.coastalpol.coastalpol.Model.JenisKriminalitas;
import com.coastalpol.coastalpol.Rest.ApiClient;
import com.coastalpol.coastalpol.Rest.ApiInterface;

import org.apache.commons.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeatmapOptionActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Button button;
    private ProgressDialog progressDialog;
    private Activity heatmapOptionActivity;
    private ApiInterface mApiInterface;
    private int jenisKriminalitasAktif;
    private int waktuAktif;
    private int bulanAktif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heatmap_option);

        // Ambil data sebelumnya
        Intent i = getIntent();
        final List<JenisKriminalitas> jenisKriminalitasList = (List<JenisKriminalitas>) i.getSerializableExtra("jenis");

        // Deklarasi variabel
        toolbar = (Toolbar) findViewById(R.id.heatmap_option_activity_toolbar);
        button = (Button) findViewById(R.id.filter_heatmap_btn);
        heatmapOptionActivity = this;
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        // Action bar
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);

        // Spinner content
        List<String> jenisArray = new ArrayList<>();
        List<String> waktuArray = new ArrayList<>();
        List<String> bulanArray = new ArrayList<>();

        jenisArray.add("Semua");

        waktuArray.add("Semua Waktu");
        waktuArray.add("00.00 s/d 05.59");
        waktuArray.add("06.00 s/d 11.59");
        waktuArray.add("12.00 s/d 17.59");
        waktuArray.add("18.00 s/d 23.59");

        bulanArray.add("Semua Bulan");
        bulanArray.add("Januari");
        bulanArray.add("Februari");
        bulanArray.add("Maret");
        bulanArray.add("April");
        bulanArray.add("Mei");
        bulanArray.add("Juni");
        bulanArray.add("Juli");
        bulanArray.add("Agustus");
        bulanArray.add("September");
        bulanArray.add("Oktober");
        bulanArray.add("November");
        bulanArray.add("Desember");

        for(int j=0; j<jenisKriminalitasList.size(); j++) {
            jenisArray.add(WordUtils.capitalize(jenisKriminalitasList.get(j).getJenis()));
        }

        ArrayAdapter<String> jenisAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                jenisArray
        );
        ArrayAdapter<String> waktuAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                waktuArray
        );
        ArrayAdapter<String> bulanAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                bulanArray
        );

        jenisAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        waktuAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        bulanAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        Spinner jenisKriminalitasS = (Spinner) findViewById(R.id.jenis_kriminalitas_sp);
        Spinner waktuKejadianS = (Spinner) findViewById(R.id.waktu_kejadian_sp);
        Spinner bulanKejadianS = (Spinner) findViewById(R.id.bulan_kejadian_sp);

        jenisKriminalitasS.setAdapter(jenisAdapter);
        waktuKejadianS.setAdapter(waktuAdapter);
        bulanKejadianS.setAdapter(bulanAdapter);

        jenisKriminalitasS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    jenisKriminalitasAktif = jenisKriminalitasList.get(i-1).getId();
                } else {
                    jenisKriminalitasAktif = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                jenisKriminalitasAktif = 0;
            }
        });
        waktuKejadianS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                waktuAktif = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                waktuAktif = 0;
            }
        });
        bulanKejadianS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bulanAktif = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                bulanAktif = 0;
            }
        });

        // Click listener
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Progress dialog
                progressDialog = new ProgressDialog(heatmapOptionActivity);

                progressDialog.setMessage("Sedang mengambil data");
                progressDialog.show();

                // Get data
                Call<List<Heatmap>> heatmapCall = mApiInterface.getHeatmap(jenisKriminalitasAktif, waktuAktif, bulanAktif);

                heatmapCall.enqueue(new Callback<List<Heatmap>>() {
                    @Override
                    public void onResponse(Call<List<Heatmap>> call, Response<List<Heatmap>> response) {
                        List<Heatmap> heatmapList = response.body();
                        Intent intent = new Intent(heatmapOptionActivity.getApplicationContext(), MapActivity.class);

                        intent.putExtra("heatmaps", (ArrayList<Heatmap>) heatmapList);
                        progressDialog.dismiss();
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<List<Heatmap>> call, Throwable t) {

                    }
                });

                Log.i("Crime GIS Heatmap", "Filter heatmap diklik dengan parameter jenis " + jenisKriminalitasAktif + " waktu " + waktuAktif + " dan bulan " + bulanAktif);
            }
        });
    }
}
