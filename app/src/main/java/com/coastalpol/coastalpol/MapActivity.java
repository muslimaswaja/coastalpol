package com.coastalpol.coastalpol;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.coastalpol.coastalpol.Model.Heatmap;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<LatLng> latLngs;
    private HeatmapTileProvider mHeatmapTileProvider;
    private TileOverlay mTileOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heatmap);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng center = new LatLng(-7.257472, 112.752088);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(center));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11.0f));

        // Deklarasi variabel
        Intent intent = getIntent();
        List<Heatmap> heatmapList = (List<Heatmap>) intent.getSerializableExtra("heatmaps");
        latLngs = parseLatLng(heatmapList);
        mHeatmapTileProvider = new HeatmapTileProvider.Builder().data(latLngs).build();
        mTileOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mHeatmapTileProvider));
    }

    private ArrayList<LatLng> parseLatLng(List<Heatmap> data) {
        ArrayList<LatLng> toReturn = new ArrayList<>();

        for(int i=0; i<data.size(); i++) {
            String[] latLng = data.get(i).getLokasi().replace("POINT(", "")
                    .replace(")","").split(" ");
            double lat = Double.parseDouble(latLng[1]);
            double lng = Double.parseDouble(latLng[0]);

            toReturn.add(new LatLng(lat,lng));
        }

        return toReturn;
    }
}
