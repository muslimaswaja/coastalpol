package com.coastalpol.coastalpol;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coastalpol.coastalpol.Model.JenisKriminalitas;
import com.coastalpol.coastalpol.Rest.ApiClient;
import com.coastalpol.coastalpol.Rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private LinearLayout menuMapLL;
    private LinearLayout menuTabelLL;
    private LinearLayout menuStatistikLL;
    private LinearLayout menuTentangLL;
    public static Activity mainActivity;
    private ApiInterface mApiInterface;
    private TextView namaAppMain;
    private Typeface circulat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Mengganti font untuk judul activity
        circulat = Typeface.createFromAsset(getAssets(), "fonts/circulat.ttf");
        namaAppMain = (TextView) findViewById(R.id.nama_app_main);

        namaAppMain.setTypeface(circulat);

        // Memfungsikan klik menu
        menuMapLL = (LinearLayout) this.findViewById(R.id.menu_map_ll);
        menuTabelLL = (LinearLayout) this.findViewById(R.id.menu_data_ll);
        menuStatistikLL = (LinearLayout) this.findViewById(R.id.menu_statistik_ll);
        menuTentangLL = (LinearLayout) this.findViewById(R.id.menu_tentang_ll);
        this.mainActivity = this;
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        menuMapLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Call<List<JenisKriminalitas>> jenisKriminalitasCall = mApiInterface.getJenisKriminalitas();
//
//                jenisKriminalitasCall.enqueue(new Callback<List<JenisKriminalitas>>() {
//                    @Override
//                    public void onResponse(Call<List<JenisKriminalitas>> call, Response<List<JenisKriminalitas>> response) {
//                        List<JenisKriminalitas> jenisKriminalitasList = response.body();

                        Intent intent = new Intent(mainActivity.getApplicationContext(), MapActivity.class);

//                        intent.putExtra("jenis", (ArrayList<JenisKriminalitas>) jenisKriminalitasList);
                        startActivity(intent);
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<JenisKriminalitas>> call, Throwable t) {
//                        Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghubungkan ke server", Toast.LENGTH_SHORT);
//
//                        Log.e("Ambil data", t.toString());
//                        toast.show();
//                    }
//                });
            }
        });
        menuTabelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity.getApplicationContext(), HeatmapOptionActivity.class);

                startActivity(intent);
            }
        });
        menuStatistikLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity.getApplicationContext(), HeatmapOptionActivity.class);

                startActivity(intent);
            }
        });
        menuTentangLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity.getApplicationContext(), AboutActivity.class);

                startActivity(intent);
            }
        });
    }
}
