package com.coastalpol.coastalpol;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.coastalpol.coastalpol.Model.FeaturesTerdekat;
import com.coastalpol.coastalpol.Model.JenisKriminalitas;
import com.coastalpol.coastalpol.Model.JenisLokasi;
import com.coastalpol.coastalpol.Model.JumlahKriminalitas;
import com.coastalpol.coastalpol.Rest.ApiClient;
import com.coastalpol.coastalpol.Rest.ApiInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckerMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    private Location mLastKnownLocation;
    private Marker myLocationMarker;
    private ImageButton myLocationIM;
    private Button mSelanjutnyaB;
    private LocationManager locationManager;
    private Toolbar toolbar;
    private CameraPosition mCameraPosition;
    public static Activity checkerMapActivity;
    private String provider;
    private String latLngString;
    private GoogleApiClient googleApiClient;
    private ApiInterface mApiInterface;
    private ProgressDialog progressDialog;
    final List<JenisKriminalitas> jenisKriminalitasList = new ArrayList<>();
    final List<List<JumlahKriminalitas>> jumlahKriminalitasList = new ArrayList<>();
    final List<FeaturesTerdekat> featuresTerdekatList = new ArrayList<>();
    final List<JenisLokasi> jenisLokasiList = new ArrayList<>();

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Surabaya, Indonesia   ) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-7.257472, 112.752088);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted = false;
    private boolean isAfterActivateGPS = false;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        setContentView(R.layout.activity_checker_map);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_checker);
        mapFragment.getMapAsync(this);

        // Inisialisasi variabel
        toolbar = (Toolbar) findViewById(R.id.checker_map_activity_toolbar);
        mSelanjutnyaB = (Button) findViewById(R.id.periksa_cm_button);
        myLocationIM = (ImageButton) findViewById(R.id.my_loc_checker_i_b);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        checkerMapActivity = this;

        // Action bar
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);

        // My Location Button
        myLocationIM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDeviceLocation();
            }
        });

        // Kirim Laporan Button
        mSelanjutnyaB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Progress dialog
                progressDialog = new ProgressDialog(checkerMapActivity);

                progressDialog.setMessage("Sedang memeriksa lokasi");
                progressDialog.show();

                // Ambil data
                ambilDataKriminalitas();
            }
        });
    }

    private void ambilDataKriminalitas() {
        Call<List<JenisKriminalitas>> jenisKriminalitasCall = mApiInterface.getJenisKriminalitas();

        jenisKriminalitasCall.enqueue(new Callback<List<JenisKriminalitas>>() {
            @Override
            public void onResponse(Call<List<JenisKriminalitas>> call, Response<List<JenisKriminalitas>> response) {
                if(response.body() != null) {
                    jenisKriminalitasList.addAll(response.body());
                    Log.i("Ambil data kriminalitas", jenisKriminalitasList.size() + "");
                    
                    if(jenisKriminalitasList.size() > 0) {
                        ambilDataLokasi();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(CheckerMapActivity.this, "Belum ada data dari server", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JenisKriminalitas>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghubungkan ke server", Toast.LENGTH_SHORT);

                Log.e("Ambil data", t.toString());
                toast.show();
            }
        });
    }

    private void ambilDataLokasi() {
        Call<List<JenisLokasi>> jenisLokasiCall = mApiInterface.getJenisLokasi();

        jenisLokasiCall.enqueue(new Callback<List<JenisLokasi>>() {
            @Override
            public void onResponse(Call<List<JenisLokasi>> call, Response<List<JenisLokasi>> response) {
                if(response.body() != null) {
                    jenisLokasiList.addAll(response.body());
                    Log.i("Ambil data lokasi", jenisLokasiList.size() + "");

                    if(jenisKriminalitasList.size() > 0) {
                        ambilJumlahKriminalitas();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(CheckerMapActivity.this, "Belum ada data dari server", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JenisLokasi>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghubungkan ke server", Toast.LENGTH_SHORT);

                Log.e("Ambil data", t.toString());
                toast.show();
            }
        });
    }

    private void ambilJumlahKriminalitas() {
        Call<List<List<JumlahKriminalitas>>> jumlahKriminalitasCall = mApiInterface.getJumlahKriminalitas(latLngString);

        jumlahKriminalitasCall.enqueue(new Callback<List<List<JumlahKriminalitas>>>() {
            @Override
            public void onResponse(Call<List<List<JumlahKriminalitas>>> call, Response<List<List<JumlahKriminalitas>>> response) {
                if(response.body() != null) {
                    jumlahKriminalitasList.addAll(response.body());
                    Log.i("Ambil data jumlah", jumlahKriminalitasList.size() + "");

                    if(jenisKriminalitasList.size() > 0) {
                        ambilFeaturesTerdekat();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(CheckerMapActivity.this, "Belum ada data dari server", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<List<JumlahKriminalitas>>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghubungkan ke server", Toast.LENGTH_SHORT);

                Log.e("Ambil data", t.toString());
                toast.show();
            }
        });
    }

    private void ambilFeaturesTerdekat() {
        Call<List<FeaturesTerdekat>> featuresTerdekatCall = mApiInterface.getFeaturesTerdekat(latLngString);

        featuresTerdekatCall.enqueue(new Callback<List<FeaturesTerdekat>>() {
            @Override
            public void onResponse(Call<List<FeaturesTerdekat>> call, Response<List<FeaturesTerdekat>> response) {
                featuresTerdekatList.addAll(response.body());
                Log.i("Ambil data features", featuresTerdekatList.size() + "");

                if(jenisKriminalitasList.size() > 0) {
                    Intent intent = new Intent(checkerMapActivity, CrimeMeterActivity.class);

                    intent.putExtra("jenisKriminalitas", (ArrayList<JenisKriminalitas>) jenisKriminalitasList);
                    intent.putExtra("jenisLokasi", (ArrayList<JenisLokasi>) jenisLokasiList);
                    intent.putExtra("jumlahKriminalitas", (ArrayList<List<JumlahKriminalitas>>) jumlahKriminalitasList);
                    intent.putExtra("featuresTerdekat", (ArrayList<FeaturesTerdekat>) featuresTerdekatList);
                    startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(CheckerMapActivity.this, "Belum ada data dari server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<FeaturesTerdekat>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghubungkan ke server", Toast.LENGTH_SHORT);

                Log.e("Ambil data", t.toString());
                toast.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
        getLocationPermission();
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                return;
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                return;
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                LatLng latLng = marker.getPosition();
                double lat = latLng.latitude;
                double lng = latLng.longitude;

                latLngString = "POINT(" + lng + " " + lat + ")";
            }
        });
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();

                            markThis(mLastKnownLocation);
                        } else {
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        AlertDialog.Builder builder = new AlertDialog.Builder(checkerMapActivity);
        AlertDialog dialog = builder.create();
        boolean locationEnabled = false;
        boolean networkEnabled = false;

        try {
            locationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.i("Lokasi", e.getMessage());
        }

        try {
            networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            Log.i("Lokasi", e.getMessage());
        }

        if (!locationEnabled && !networkEnabled) {
            final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            mLocationPermissionGranted = false;

            builder.setMessage("Layanan lokasi Anda sedang tidak aktif. Fitur ini membutuhkan akses layanan lokasi. \n\nAktifkan layanan lokasi?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    isAfterActivateGPS = true;
                    startActivity(intent);
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(checkerMapActivity.getApplicationContext(), MainActivity.class);

                    startActivity(intent);
                    finish();
                }
            });

            dialog = builder.create();

            dialog.show();
        } else {
            mLocationPermissionGranted = true;

            getDeviceLocation();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isAfterActivateGPS) {
            isAfterActivateGPS = false;
            getLocationPermission();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cari_tempat:
                showSearchIntent();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSearchIntent() {
        try {
            LatLng southwest = new LatLng(-7.356303, 112.612110);
            LatLng northeast = new LatLng(-7.196241, 112.855869);
            LatLngBounds latLngBounds = new LatLngBounds(southwest, northeast);

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setBoundsBias(latLngBounds)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e("Google Play", e.toString());
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("Google Play", e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);

                markThis(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                Log.e("Lokasi", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("Lokasi", "Result cancelled");
            }
        }
    }

    private void markThis(Location location) {
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            LatLng latLng = new LatLng(lat, lng);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(lat, lng), DEFAULT_ZOOM));

            if (myLocationMarker != null) {
                myLocationMarker.remove();
            }

            myLocationMarker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
            latLngString = "POINT(" + lng + " " + lat + ")";
        } else {
            String[] latLongDump = mDefaultLocation.toString()
                    .replace("lat/lng: (", "")
                    .replace(")", "").split(",");
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    mDefaultLocation, DEFAULT_ZOOM));

            if (myLocationMarker != null) {
                myLocationMarker.remove();
            }

            myLocationMarker = mMap.addMarker(new MarkerOptions().position(mDefaultLocation).draggable(true));
            latLngString = "POINT(" + latLongDump[1] + " " + latLongDump[0] + ")";
        }
    }

    private void markThis(Place place) {
        if (place != null) {
            double lat = place.getLatLng().latitude;
            double lng = place.getLatLng().longitude;
            LatLng latLng = new LatLng(lat, lng);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(lat, lng), DEFAULT_ZOOM));

            if (myLocationMarker != null) {
                myLocationMarker.remove();
            }

            myLocationMarker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
            latLngString = "POINT(" + lng + " " + lat + ")";
        } else {

            double lat = mDefaultLocation.latitude;
            double lng = mDefaultLocation.longitude;

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    mDefaultLocation, DEFAULT_ZOOM));

            if (myLocationMarker != null) {
                myLocationMarker.remove();
            }

            myLocationMarker = mMap.addMarker(new MarkerOptions().position(mDefaultLocation).draggable(true));
            latLngString = "POINT(" + lat + " " + lat + ")";
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {}

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
}
