package com.coastalpol.coastalpol.Rest;

import com.coastalpol.coastalpol.Model.FeaturesTerdekat;
import com.coastalpol.coastalpol.Model.Heatmap;
import com.coastalpol.coastalpol.Model.JenisKriminalitas;
import com.coastalpol.coastalpol.Model.JenisLokasi;
import com.coastalpol.coastalpol.Model.JumlahKriminalitas;
import com.coastalpol.coastalpol.Model.LaporanKriminalitas;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("jenis-kriminalitas")
    Call<List<JenisKriminalitas>> getJenisKriminalitas();

    @GET("grafik-kriminalitas")
    Call<List<List<JumlahKriminalitas>>> getJumlahKriminalitas(@Query("koordinat") String koordinat);

    @GET("features-terdekat")
    Call<List<FeaturesTerdekat>> getFeaturesTerdekat(@Query("koordinat") String koordinat);

    @GET("jenis-lokasi")
    Call<List<JenisLokasi>> getJenisLokasi();

    @GET("heatmap")
    Call<List<Heatmap>> getHeatmap(@Query("jenis") int jenis, @Query("waktu") int waktu, @Query("bulan") int bulan);

    @POST("laporan-kriminal")
    Call<Boolean> postLaporan(@Body LaporanKriminalitas laporanKriminalitas);
}
